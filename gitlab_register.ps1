$cfg_name = Get-Content C:\GitLab-Runner\config.toml | Where-Object {$_ -match 'name = '}
#$cfg_name = Get-Content C:\Work\AWS\EC2ImageBuilder\config.toml | Where-Object {$_ -match 'name = '}

# Get substring (token) and system hostname
$fi = $cfg_name.IndexOf('"')
$li = $cfg_name.LastIndexOf('"')
$name1 = $cfg_name.Substring($fi+1, $li-$fi-1)
$name2 = hostname

echo "GitLab Runner cfg hostname: $name1"
echo "Windows hostname: $name2"

if ( $name1 -ne $name2 )
{
    # hostname does not match the name found in GitLab Runner cfg, reregister...
    echo "hostname does not match the name found in GitLab Runner cfg, reregister..."
    cd C:\GitLab-Runner\
    $regtoken = [System.IO.File]::ReadLines("C:\GitLab-Runner\tok.en")
    Remove-Item -Path C:\GitLab-Runner\config.toml
    C:\GitLab-Runner\gitlab-runner.exe register -n -r $regtoken --tag-list "gr-win" --maintenance-note "GitLab Runner Win2022" -u https://gitlab.com --executor shell

}
